# ホームページをAlgoliaに入れてくれるやつ

sitemap.xml に掲載されているURL を取得してAlgolia に検索用のIndexを作ります。

## つかいかた

```sh
ruby main.rb https://example.com/sitemap.xml indexName
```

indexName で指定したindexを一旦クリアして、sitemap.xml に掲載されたURLを取得した結果を入れていきます。
一旦クリアしてしまうので、サービス影響無く実施するにはindexをblue/greenする対応が必要です。

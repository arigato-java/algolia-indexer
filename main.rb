# frozen_string_literal: true

require 'bundler/setup'
require 'algolia'
require 'net/http'
require 'nokogiri'
require 'open-uri'
require 'algolia_html_extractor'

def extract_urls_from_sitemap(xml)
  doc = Nokogiri::XML(xml)
  urls = doc.xpath('//xmlns:url/xmlns:loc/text()', 'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9')
  urls.map(&:text)
end

def handle_plaintext(text_content)
  [text_content]
end

urls = extract_urls_from_sitemap(URI.parse(ARGV[0]).open)
extractor_options = AlgoliaHTMLExtractor.default_options(
  {
    css_selector: 'p,div,li,dt,dd,th,td,pre'
  }
)

client = Algolia::Search::Client.create(ENV['ALGOLIA_APP_ID'], ENV['ALGOLIA_SECRET'])
index = client.init_index(ARGV[1])
index.clear_objects

urls.each do |url|
  uri = URI.parse(url)
  puts uri.path
  response = Net::HTTP.get_response(uri)
  content_type = response['Content-Type']
  case content_type
  when %r{^text/html}
    records = AlgoliaHTMLExtractor.run(response.body, options: extractor_options)
    objects = records.map do |record|
      {
        'objectID': record[:objectID],
        'content': record[:content],
        'headings': record[:headings],
        'path': uri.path
      }
    end
    index.save_objects(objects)
  end
rescue StandardError => e
  puts e
end
